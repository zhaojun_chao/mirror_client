#ifndef AES_TCP_LIB_GLOBAL_H
#define AES_TCP_LIB_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(AES_TCP_LIB_LIBRARY)
#  define AES_TCP_LIB_EXPORT Q_DECL_EXPORT
#else
#  define AES_TCP_LIB_EXPORT Q_DECL_IMPORT
#endif

#endif // AES_TCP_LIB_GLOBAL_H
