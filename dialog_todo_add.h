#ifndef DIALOG_TODO_ADD_H
#define DIALOG_TODO_ADD_H

#include <QDialog>

namespace Ui {
class Dialog_todo_add;
}

class Dialog_todo_add : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog_todo_add(QWidget *parent = nullptr);
    ~Dialog_todo_add();
    QString todo;

private slots:
    void on_pushButton_add_clicked();

private:
    Ui::Dialog_todo_add *ui;
};

#endif // DIALOG_TODO_ADD_H
