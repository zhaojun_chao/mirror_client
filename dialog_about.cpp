#include "dialog_about.h"
#include "ui_dialog_about.h"

Dialog_about::Dialog_about(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog_about)
{
    ui->setupUi(this);
    fb_info.clear();
}

Dialog_about::~Dialog_about()
{
    delete ui;
}

void Dialog_about::on_pushButton_send_clicked()
{


    fb_info = ui->textEdit->document()->toPlainText();

    if(!fb_info.isEmpty()){

        close();

    }


}
