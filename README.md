# 说明：
 ### 本软件是哇咔咔魔镜配套的PC用户端，用于添加、删除哇咔咔魔镜的待办事项，通过串口设置哇咔咔魔镜的运行参数


# 魔镜功能：
## 1. 时间显示  
## 2. 天气显示  
## 3. 温湿度、空气状况
## 4. 待办事项


# 指令说明：

## 用户端：
1. 添加一条待办事项  
"<user_msg type=\"mirror_msg\" body=\"[todo]\"  flag=\"todo_add\"/>"  
body 字段中内容需base64编码后填入
2. 删除一条待办  
<user_msg type=\"mirror_msg\" body=\"[id]\"  flag=\"todo_del\"/>  

3. 请求当前待办列表  
<user_msg type=\"mirror_msg\" body=\"requst_list\"  flag=\"todo_list\"/>    

## 魔镜端：
1. 响应 requst_list
<user_msg type=\"mirror_msg\" body=\"[todo list]\"  flag=\"todo_list_re\"/>  
body 字段中内容需base64解码，每条待办事项之间用‘#’分割


