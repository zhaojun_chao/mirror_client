#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QSerialPort>
#include <QSerialPortInfo>
#include <QSystemTrayIcon>
#include "aes_tcp_lib.h"


#include "dialog_todo_add.h"
#include "dialog_set.h"
#include "dialog_about.h"

#include <QTimer>
#include <QCloseEvent>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    void flush_todo(void);

    Dialog_todo_add *mirror_todo_add = new Dialog_todo_add;
    Dialog_set * mirror_set_window = new Dialog_set;
    Dialog_about * mirror_about_window = new Dialog_about;

    aes_tcp_lib *lus_tcp;
    QTimer *timer =new QTimer;


    void load_set(void);
    QString ini_fix_id;

    QSystemTrayIcon *mSysTrayIcon;
    QMenu *mMenu;
    QAction *mShowMainAction;
    QAction *mExitAppAction;

    void createActions();
    void createMenu();

private slots:

    void connect_flush(void);

    void mirror_flush_man(bool);
    void todo_del(bool);
    void mirror_set(bool);
    void mirror_about(bool);
    void todo_add(bool);

    void do_msg(QByteArray msg);
    void public_timer_fun();

    void on_tableWidget_customContextMenuRequested(const QPoint &pos);

    void on_activatedSysTrayIcon(QSystemTrayIcon::ActivationReason reason);
    void on_showMainAction();
    void on_exitAppAction();

private:
    Ui::MainWindow *ui;

    QSerialPort * serial = new QSerialPort;
    void mirror_uartwrite_set();

    void flush_mirror_faaddr();

protected:
    void closeEvent(QCloseEvent *event);//重写窗口关闭事件

};

#endif // MAINWINDOW_H
