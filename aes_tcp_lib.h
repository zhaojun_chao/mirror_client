#ifndef AES_TCP_LIB_H
#define AES_TCP_LIB_H

#include <QtNetwork>
#include <QDebug>
#include <QObject>
#include <QtXml>
#include <QtXml/QDomDocument>
#include <QString>
#include <openssl/rsa.h>
#include "aes_tcp_lib_global.h"


#define ENCRYPT_YES 0x12
#define ENCRYPT_NO	0x7

#define BIG		1
#define LITTLE	2

enum DOWN_STATUS{
    DOWN_ING= 0,
    DOWN_OK
};

struct lus_msg{

    QByteArray msg;
    QByteArray to_addr;
    quint8 encrypt_c;
};


class AES_TCP_LIB_EXPORT aes_tcp_lib:public QObject
{
   Q_OBJECT
public:
    static aes_tcp_lib * create_aes_tcp();
    void aes_tcp_init(void);
    void aes_tcp_close(void);
    void lus_set_order(qint8 order);
    void update_fix_id();
    void sendMessage(QByteArray b_data, quint8 encrypt_c);
    void prase_terminal_cmd(QDomElement rootnode);
    void lus_send_user_msg(QString type, QString to_addr, QString body);
    void lus_send_raw_msg(QByteArray b_data, quint8 encrypt_c);
    void lus_down_pool_file(QString index, QString label, QString file_name, QString save_path="./");
    void lus_set_debug(bool sw);
    void lus_set_server(QString ip, quint16 port);
    void lus_set_prase(bool sw);
    void lus_set_dst(QString addr);
    QString lus_get_fix_id(void);
    QString lus_get_var_id(void);
    uint8_t lus_get_down_status(void);

private slots:

      void displayError(QAbstractSocket::SocketError);  //显示错误
      void readMessage_msg();
     // void readMessage_big();
     // void readMessage(uint8_t select);
      void f_connected(void);
      void enable_reconnect(void);
      void disable_reconnect(void);
      void timer_auto_connect(void);
      void timer_heart_keep(void);
      void do_prase_requst(QByteArray msg);
    void updateServerProgress();

signals:

     void aes_tcp_connected(void);
     void aes_tcp_disconnected(void);
     void read_xml_msg(QDomElement rootnode);
     void read_msg(QByteArray msg);
     void recive_lus_pack(QString file_name);
     void image_ready(QImage *image);
     void down_status_change(uint8_t status);
};


#endif // AES_TCP_LIB_H
