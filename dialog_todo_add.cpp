#include "dialog_todo_add.h"
#include "ui_dialog_todo_add.h"

Dialog_todo_add::Dialog_todo_add(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog_todo_add)
{
    ui->setupUi(this);
}

Dialog_todo_add::~Dialog_todo_add()
{
    delete ui;
}

void Dialog_todo_add::on_pushButton_add_clicked()
{

    todo = ui->lineEdit->text();
    ui->lineEdit->clear();
    close();

}
