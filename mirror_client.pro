#-------------------------------------------------
#
# Project created by QtCreator 2019-11-06T16:59:44
#
#-------------------------------------------------

QT      += core gui
QT	+= network
QT	+= xml
QT	+= serialport

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = mirror_client
TEMPLATE = app
RC_ICONS = log_l.ico

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11

SOURCES += \
        dialog_todo_add.cpp \
        main.cpp \
        mainwindow.cpp \
    dialog_set.cpp \
    dialog_about.cpp

HEADERS += \
    aes_tcp_lib.h \
    aes_tcp_lib_global.h \
        dialog_todo_add.h \
        mainwindow.h \
    dialog_set.h \
    dialog_about.h

FORMS += \
        dialog_todo_add.ui \
        mainwindow.ui \
    dialog_set.ui \
    dialog_about.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    res.qrc

unix:!macx: LIBS += -L$$PWD/lib/ -laes_tcp_lib

INCLUDEPATH += $$PWD/.
DEPENDPATH += $$PWD/.
