#include "dialog_set.h"
#include "ui_dialog_set.h"
#include <QSettings>
#include <QMessageBox>

Dialog_set::Dialog_set(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog_set)
{
    ui->setupUi(this);
   // ui->lineEdit_wireless_pw->setDisabled(true);
   //ui->lineEdit_wireless_id->setDisabled(true);

    QSettings set("mirror_set.ini", QSettings::IniFormat);
    key = set.value("main/fix_id").toString();
    ui->lineEdit_fix_id->setText(key);
}

Dialog_set::~Dialog_set()
{
    delete ui;
}

void Dialog_set::on_pushButton_add_key_clicked()
{

    if(!ui->lineEdit_fix_id->text().isEmpty()){

        QSettings settings("mirror_set.ini", QSettings::IniFormat);

        settings.beginGroup("main");

        settings.setValue("fix_id",ui->lineEdit_fix_id->text());

        settings.endGroup();

        QMessageBox::information(this, "提示", "设置成功", QMessageBox::Ok);

        close();

    }

    QString w_ssid = ui->lineEdit_wireless_id->text();
    QString    w_pw 	 = ui->lineEdit_wireless_pw->text();
    if((!w_ssid.isEmpty()) && (!w_pw.isEmpty())){

        QString wifi_cmd;
        wifi_cmd = "<mirror_set type=\"wifi_set\" ssid=\"arg_1\" passwd=\"arg_2\" />";
        wifi_cmd.replace("arg_1", w_ssid);
        wifi_cmd.replace("arg_2", w_pw);

        set_cmd_list.append(wifi_cmd);

    }
}
